<p></p>
<p></p>

<p align="center">
<img src="resources/icons/logo.png" height="80"/> 
</p>


<div align="center">

[![Stars](https://gitee.com/py2cn/pyminer/badge/star.svg?theme=gvp)](https://gitee.com/py2cn/pyminer/stargazers)
[![Platform](https://img.shields.io/badge/python-v3.8-blue)](https://img.shields.io/badge/python-v3.8-blue)
[![Platform](https://img.shields.io/badge/PySide2-blue)](https://img.shields.io/badge/PySide2-blue)
[![License](https://img.shields.io/badge/license-LGPL-blue)](https://img.shields.io/badge/license-LGPL-blue)

</div>

<div align="center">
    <a src="https://img.shields.io/badge/QQ%e7%be%a4-orange">
        <img src="https://img.shields.io/badge/QQ%e7%be%a4-945391275-orange">
    </a>
</div>

<p></p>
<p></p>

<div align="center">
`pyminer` 基于Python的开源、跨平台数据计算解决方案。

</div>
pyminer 是一款基于Python的开源、跨平台数据计算解决方案，通过加载各种插件实现不同的需求，用易于操作的形式，在统一的界面中，通过数据计算实现用户所设想的任务。

<p></p>
<p></p>

## 🔈 声明
pyminer 一直是一个开放、包容的开源项目，项目中的工具方法来源也非常广泛，因此允许并鼓励任何人无条件可以：

1. 将pyminer用于商业、培训等任何合法场景；
2. 复制、修改 pyminer中的任意代码且无需声明；
3. 复制修改 pyminer官方文档；
4. 鼓励自行写作 PyMiner 相关的书籍、博客、文档等内容（收费也可）；
5. 鼓励播主、培训机构培训 pyminer工具的任何内容（收费也可）；

pyminer希望成为一个伟大的开源项目，也希望得到大家的认可和赞美，仅此而已。

<p></p>
<p></p>

## 🎉 技术说明

1. 项目开发环境支持跨平台，windows,linux,mac 都支持。
2. Python版本：支持Python3.5及以上，但建议使用Python3.8及以上版本，性能更好。
3. Qt的Python接口：使用PySide2，版本为5.15.2。
4. 项目开发环境使用PyCharm

注意：

- pyminer 的官方发行版本为Python3.8+PySide2-5.15.2。开发者可自行使用其他版本的Python解释器配置相关环境。
- pyminer 曾经由PyQt5开发。但考虑到官方支持以及许可证的类型，我们已经迁移到了PySide2并改变许可证为LGPL。请勿使用PyQt5安装。
- 当使用Python3.8配置环境时，不支持3.8.0等低版本的Python3.8解释器。当使用Python3.8时，请使用3.8.5或者更高版本的解释器。

- 如果使用出现问题，欢迎提issue。



<p></p>
<p></p>
<img src="resources/images/PyMiner框架说明.jpg" width = "1000" height = "550" alt="QQ群" align=center />

<p></p>
<p></p>

## 🎁 文档地址
- 项目文档：[https://gitee.com/py2cn/pyminer/wikis](https://gitee.com/py2cn/pyminer/wikis)
- API文档：[http://py2cn.gitee.io/pyminer](http://py2cn.gitee.io/pyminer)
- MATLAB与Numpy对比：[http://mathesaurus.sourceforge.net/matlab-numpy.html](http://mathesaurus.sourceforge.net/matlab-numpy.html)

<p></p>
<p></p>

## ⏳ 当前进度
[https://gitee.com/py2cn/pyminer/board](https://gitee.com/py2cn/pyminer/board)


## 🚄 开源地址

- Gitee：[https://gitee.com/py2cn/pyminer](https://gitee.com/py2cn/pyminer)
- GitHub：[https://github.com/aboutlong/pyminer](https://github.com/aboutlong/pyminer)

<p></p>
<p></p>

## 🥂 安装体验

<p></p>
<p></p>

### 发行版下载（仅Windows系统）
我们为Windows系统的用户提供了发行版的下载链接，你可以在我们的官网中下载发行版即刻体验。对于Mac OS和Linux系统的用户，暂时不提供发行版，可以参阅“开发者自行安装”一节。

官网链接：[请点击这里打开](http://www.pyminer.com/)

### 开发者自行安装（适合Windows、Mac OS以及各大Linux发行版）
#### 安装前准备：
1. 确认你的Python解释器版本。pyminer支持3.5~3.9。
	- 当使用Python3.8.x时，建议x>=5,也就是使用Python3.8.5及以上版本，否则安装PySide2可能遇到问题
	- 3.5.x/3.6.x/3.7.x/3.9.x下，由于开发人员不足，未进行充分测试。为稳定起见，建议解释器版本x>=5。
2. 建议新建全新的虚拟环境，尤其是当旧的虚拟环境中存在其他依赖于PyQt/PySide2的程序时。pyminer与这些程序可能发生依赖冲突。


#### Windows安装

```bash
#第一步：下载源代码
git clone https://gitee.com/py2cn/pyminer.git
#第二步：同步安装依赖包和PyMiner，如果遇到安装失败的情况需要手动安装
python -m pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements.txt
#第三步：运行主程序
python app2.py
```

#### Linux/Mac OS安装

```bash
#第一步：下载源代码
git clone https://gitee.com/py2cn/pyminer.git
#第二步：同步安装依赖包和PyMiner，如果遇到安装失败的情况需要手动安装
python3 -m pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements_linux.txt
#第三步：运行主程序
python3 app2.py
```

## 📱 加入我们

作者：pyminer development team

邮箱：team@py2cn.com

欢迎各位开发者大佬加入 

<p></p>
<p></p>

<img src="resources/screenshot/group.jpg" width = "300" height = "500" alt="QQ群" align=center />

<p></p>
<p></p>

## 📱 支持我们
<div align="center">
<img src="resources/images/weixin.png" width = "300" height = "500" alt="微信支付" align=left />
<img src="resources/images/zhifubao.png" width = "300" height = "500" alt="支付宝"  />
</div>


##  🚥 许可说明
本项目遵循LGPL许可证。

许可解释权归属 pyminer development team。

##  📸 预览截图

基本界面
![avatar](resources/screenshot/main.png)

代码提示
![avatar](resources/screenshot/code.png)

绘图
![avatar](resources/screenshot/check_data.png)

