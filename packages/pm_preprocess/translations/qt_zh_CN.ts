<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="zh_CN">
<context>
    <name>BaseDFOperationDialog</name>
    <message>
        <location filename="base.py" line="35"/>
        <source>Preview</source>
        <translation>预览</translation>
    </message>
    <message>
        <location filename="base.py" line="36"/>
        <source>Save to Var</source>
        <translation>保存到变量</translation>
    </message>
    <message>
        <location filename="base.py" line="37"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="base.py" line="38"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
</context>
<context>
    <name>CLASS_NAME</name>
    <message>
        <location filename="dropna.py" line="16"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="dropna.py" line="16"/>
        <source>No data in workspace</source>
        <translation>工作空间没有数据</translation>
    </message>
</context>
<context>
    <name>DropNADialog</name>
    <message>
        <location filename="dropna.py" line="24"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="dropna.py" line="25"/>
        <source>How to drop</source>
        <translation>去除方式</translation>
    </message>
    <message>
        <location filename="dropna.py" line="25"/>
        <source>All</source>
        <translation>所有</translation>
    </message>
    <message>
        <location filename="dropna.py" line="25"/>
        <source>Any</source>
        <translation>任意值</translation>
    </message>
    <message>
        <location filename="dropna.py" line="27"/>
        <source>Drop by subset</source>
        <translation>按列去除</translation>
    </message>
    <message>
        <location filename="dropna.py" line="28"/>
        <source>Subset</source>
        <translation>去除缺失值的列</translation>
    </message>
    <message>
        <location filename="dropna.py" line="29"/>
        <source>In Place</source>
        <translation>在本变量上更改</translation>
    </message>
    <message>
        <location filename="dropna.py" line="38"/>
        <source>Drop NA:</source>
        <translation>去除缺失值：</translation>
    </message>
    <message>
        <location filename="dropna.py" line="24"/>
        <source>&#xe8;&#xa1;&#x8c;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dropna.py" line="24"/>
        <source>&#xe5;&#x88;&#x97;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FillNADialog</name>
    <message>
        <location filename="fillna.py" line="37"/>
        <source>Replace Policy</source>
        <translation>替换策略</translation>
    </message>
    <message>
        <location filename="fillna.py" line="38"/>
        <source>User Defined Value</source>
        <translation>自定义值</translation>
    </message>
    <message>
        <location filename="fillna.py" line="38"/>
        <source>Fill Front or Back</source>
        <translation>以前或后值填充</translation>
    </message>
    <message>
        <location filename="fillna.py" line="39"/>
        <source>Value to replace</source>
        <translation>要替换的值</translation>
    </message>
    <message>
        <location filename="fillna.py" line="40"/>
        <source>Fill Direction</source>
        <translation>填充方向</translation>
    </message>
    <message>
        <location filename="fillna.py" line="41"/>
        <source>Front Fill</source>
        <translation>以前一个值填充</translation>
    </message>
    <message>
        <location filename="fillna.py" line="41"/>
        <source>Back Fill</source>
        <translation>以后一个值填充</translation>
    </message>
    <message>
        <location filename="fillna.py" line="42"/>
        <source>Fill Axis</source>
        <translation>填充轴</translation>
    </message>
    <message>
        <location filename="fillna.py" line="42"/>
        <source>By Row</source>
        <translation>按行填充</translation>
    </message>
    <message>
        <location filename="fillna.py" line="43"/>
        <source>By Column</source>
        <translation>按列填充</translation>
    </message>
    <message>
        <location filename="fillna.py" line="45"/>
        <source>Limit Maximum Fills</source>
        <translation>限制每行/每列最大填充数量</translation>
    </message>
    <message>
        <location filename="fillna.py" line="46"/>
        <source>Maximum Fills</source>
        <translation>最大填充数量</translation>
    </message>
    <message>
        <location filename="fillna.py" line="47"/>
        <source>In Place</source>
        <translation>在本变量上更改</translation>
    </message>
    <message>
        <location filename="fillna.py" line="57"/>
        <source>Fill NA:</source>
        <translation>填充缺失值：</translation>
    </message>
</context>
<context>
    <name>MergeDialog</name>
    <message>
        <location filename="datamerge.py" line="45"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="datamerge.py" line="36"/>
        <source>You can merge at most 6 data frames.</source>
        <translation>最多可以合并6个数据集</translation>
    </message>
    <message>
        <location filename="datamerge.py" line="45"/>
        <source>No Data found in workspace!</source>
        <translation>工作空间中无变量！</translation>
    </message>
    <message>
        <location filename="datamerge.py" line="57"/>
        <source>Import Data, code: %s</source>
        <translation>导入数据，代码为：%s</translation>
    </message>
</context>
<context>
    <name>PMPreprocessToolBar</name>
    <message>
        <location filename="main.py" line="51"/>
        <source>Find/Replace</source>
        <translation>查找/替换</translation>
    </message>
    <message>
        <location filename="main.py" line="53"/>
        <source>Data Info</source>
        <translation>数据信息</translation>
    </message>
    <message>
        <location filename="main.py" line="55"/>
        <source>Column</source>
        <translation>列名</translation>
    </message>
    <message>
        <location filename="main.py" line="58"/>
        <source>Data Role</source>
        <translation>数据角色</translation>
    </message>
    <message>
        <location filename="main.py" line="63"/>
        <source>New Row</source>
        <translation>新增行</translation>
    </message>
    <message>
        <location filename="main.py" line="65"/>
        <source>New Column</source>
        <translation>新增列</translation>
    </message>
    <message>
        <location filename="main.py" line="67"/>
        <source>Delete Row</source>
        <translation>删除行</translation>
    </message>
    <message>
        <location filename="main.py" line="69"/>
        <source>Delete Column</source>
        <translation>删除列</translation>
    </message>
    <message>
        <location filename="main.py" line="74"/>
        <source>Missing Value</source>
        <translation>缺失值处理</translation>
    </message>
    <message>
        <location filename="main.py" line="76"/>
        <source>Sample</source>
        <translation>抽样</translation>
    </message>
    <message>
        <location filename="main.py" line="78"/>
        <source>Transpose</source>
        <translation>转置</translation>
    </message>
    <message>
        <location filename="main.py" line="81"/>
        <source>Vertical Merger</source>
        <translation>纵向合并</translation>
    </message>
    <message>
        <location filename="main.py" line="83"/>
        <source>Horizontal Merger</source>
        <translation>横向合并</translation>
    </message>
    <message>
        <location filename="main.py" line="86"/>
        <source>Join</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="main.py" line="88"/>
        <source>Normalization</source>
        <translation>标准化</translation>
    </message>
    <message>
        <location filename="main.py" line="96"/>
        <source>Preprocess</source>
        <translation>数据处理</translation>
    </message>
    <message>
        <location filename="main.py" line="49"/>
        <source>Filter</source>
        <translation>筛选</translation>
    </message>
    <message>
        <location filename="main.py" line="60"/>
        <source>Data Partition</source>
        <translation>数据分区</translation>
    </message>
    <message>
        <location filename="main.py" line="72"/>
        <source>Drop Missing Value</source>
        <translation>去除缺失值</translation>
    </message>
    <message>
        <location filename="main.py" line="74"/>
        <source>Fill Missing Value</source>
        <translation>填充缺失值</translation>
    </message>
</context>
<context>
    <name>TransposeDialog</name>
    <message>
        <location filename="transpose.py" line="21"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="transpose.py" line="29"/>
        <source>Transpose, command: </source>
        <translation>转置，命令：</translation>
    </message>
</context>
</TS>
