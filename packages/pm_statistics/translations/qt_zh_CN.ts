<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Form</name>
    <message>
        <location filename="../ui/stat_base.py" line="222"/>
        <source>显示描述性统计量</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="223"/>
        <source>全部变量:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="224"/>
        <source>已选变量:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="225"/>
        <source>分组变量(可选)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="226"/>
        <source>基本</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="227"/>
        <source>全选</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="228"/>
        <source>自定义</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="229"/>
        <source>计数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="230"/>
        <source>非缺失值计数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="231"/>
        <source>缺失值计数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="232"/>
        <source>缺失值占比</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="233"/>
        <source>唯一值</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="234"/>
        <source>唯一占比</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="235"/>
        <source>最大值</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="236"/>
        <source>最小值</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="237"/>
        <source>总和</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="238"/>
        <source>均值</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="239"/>
        <source>众数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="240"/>
        <source>峰度</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="241"/>
        <source>偏度</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="242"/>
        <source>方差</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="243"/>
        <source>标准差</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="244"/>
        <source>均值标准误</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="245"/>
        <source>下四分位数Q1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="246"/>
        <source>中位数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="247"/>
        <source>上四分位数Q3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="248"/>
        <source>四分位间距</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="249"/>
        <source>极差</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="250"/>
        <source>变异系数</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="251"/>
        <source>平方和</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="252"/>
        <source>选项</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="253"/>
        <source>数据直方图</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="254"/>
        <source>带正态曲线的数据直方图</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="255"/>
        <source>单值图</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="256"/>
        <source>数据箱线图</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="257"/>
        <source>图形</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="258"/>
        <source>帮助</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="259"/>
        <source>确定</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/stat_base.py" line="260"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PMStatsToolBar</name>
    <message>
        <location filename="../main.py" line="35"/>
        <source>Descriptive Statistics</source>
        <translation>描述统计</translation>
    </message>
    <message>
        <location filename="../main.py" line="37"/>
        <source>Bayesian Statistics</source>
        <translation>贝叶斯统计</translation>
    </message>
    <message>
        <location filename="../main.py" line="40"/>
        <source>Compare Means</source>
        <translation>比较平均值</translation>
    </message>
    <message>
        <location filename="../main.py" line="42"/>
        <source>Missing Value Analysis</source>
        <translation>缺失值分析</translation>
    </message>
    <message>
        <location filename="../main.py" line="45"/>
        <source>General Linear Model</source>
        <translation>一般线性模型</translation>
    </message>
    <message>
        <location filename="../main.py" line="47"/>
        <source>Generalized Linear Models</source>
        <translation>广义线性模型</translation>
    </message>
    <message>
        <location filename="../main.py" line="50"/>
        <source>Correlate</source>
        <translation>相关</translation>
    </message>
    <message>
        <location filename="../main.py" line="52"/>
        <source>Regression</source>
        <translation>回归</translation>
    </message>
    <message>
        <location filename="../main.py" line="54"/>
        <source>Classify</source>
        <translation>分类</translation>
    </message>
    <message>
        <location filename="../main.py" line="58"/>
        <source>Dimension Reduction</source>
        <translation>降维</translation>
    </message>
    <message>
        <location filename="../main.py" line="67"/>
        <source>Stats</source>
        <translation>统计</translation>
    </message>
    <message>
        <location filename="../main.py" line="60"/>
        <source>Survival</source>
        <translation>生存分析</translation>
    </message>
</context>
</TS>
