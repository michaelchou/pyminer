# -*- coding:utf-8 -*-
# @Time: 2021/1/30 18:11
# @Author: Zhanyi Hou
# @Email: 1295752786@qq.com
# @File: __init__.py.py

from packages.code_editor.codeeditor.qtpyeditor.codeeditor.baseeditor import PMGBaseEditor
from packages.code_editor.codeeditor.qtpyeditor.codeeditor.pythoneditor import PMGPythonEditor