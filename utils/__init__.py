# -*- coding: utf-8 -*-
# Copyright (c) 2020 PyMiner Development Team <team@py2cn.com>
# 
# 通用工具，可以在全局进行调用。

import os
import datetime
import logging
import webbrowser

from .path import (
    get_root_dir,
    get_user_dir,
    get_desktop_dir,
    get_documents_dir,
)
from .environ import (
    get_python_version,
    get_python_modules_directory,
    getPySideModulesDirectory,
    getScriptsPath,
    getDesignerPath
)
from .platform import (
    is_windows_platform,
    is_mac_platform,
    is_linux_platform,
    is_kde_desktop,
    is_gnome_desktop
)

from .io import *
from .debug import *
from .ui import *

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import pmgui
version = 'v2.1.0 Beta'

_application = None
_root_dir = None
_main_window: Optional["pmgui.MainWindow"] = None


def get_application() -> None:
    """
    获取QApplication
    Returns:

    """
    assert _application is not None
    return _application


def get_main_window() -> Optional["pmgui.MainWindow"]:
    """
    获取主窗口或者主控件。
    Returns:
    """
    return _main_window


def get_work_dir() -> 'str':
    """
    获取主窗口或者主控件。
    Returns:
    """
    from features.io.settings import Settings
    return Settings.get_instance()['work_dir']


def open_url(url):
    """
    打开网址
    """
    try:
        webbrowser.get('chrome').open_new_tab(url)
    except Exception as e:
        webbrowser.open_new_tab(url)


def unzip_file(zip_src: str, dst_dir: str):
    """
    解压文件
    Args:
        zip_src:
        dst_dir:

    Returns:

    """
    r = zipfile.is_zipfile(zip_src)
    if r:
        fz = zipfile.ZipFile(zip_src, 'r')
        for file in fz.namelist():
            fz.extract(file, dst_dir)
    else:
        print('This is not zip')


def make_zip(src_path, zip_dist_path, root='', rules=None):
    """
    创建zip包
    Args:
        src_path:
        zip_dist_path:
        root:
        rules:

    Returns:

    """
    if rules is None:
        rules = []
    z = zipfile.ZipFile(zip_dist_path, 'w', zipfile.ZIP_DEFLATED)
    for dirpath, dirnames, filenames in os.walk(src_path):
        relpath = os.path.relpath(dirpath, src_path)
        if is_neglect_path(relpath, rules):
            continue
        fpath = os.path.relpath(dirpath, src_path)
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            z.write(filepath, os.path.join(root, fpath, filename))
    z.close()
