# -*- coding: utf-8 -*-
# Copyright (c) 2020 pyminer development team <team@py2cn.com>
# Get application path information
# 获取应用路径信息
import os
import sys
from PySide2.QtCore import QStandardPaths


def get_root_dir() -> str:
    """
    获取根路径。
    Returns:

    """
    path = os.path.dirname(os.path.dirname(__file__))
    return path


def get_user_dir() -> str:
    """
    获取用户目录
    """
    path = os.path.expanduser('~')
    return path


def get_desktop_dir() -> str:
    """
    获取用户目录
    """
    path = os.path.join(os.path.expanduser('~'), 'Desktop')
    return path


def get_documents_dir() -> str:
    """
    获取用户目录
    """
    path = os.path.join(os.path.expanduser('~'), 'Documents')
    return path


def get_path_desktop() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.DesktopLocation)
    return path


def get_path_documents() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.DocumentsLocation)
    return path


def get_path_fonts() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.FontsLocation)
    return path


def get_path_applications() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.ApplicationsLocation)
    return path


def get_path_music() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.MusicLocation)
    return path


def get_path_movies() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.MoviesLocation)
    return path


def get_path_pictures() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.PicturesLocation)
    return path


def get_path_temp() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.TempLocation)
    return path


def get_path_home() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.HomeLocation)
    return path


def get_path_data() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.DataLocation)
    return path


def get_path_cache() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.CacheLocation)
    return path


def get_path_generic_data() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.GenericDataLocation)
    return path


def get_path_runtime() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.RuntimeLocation)
    return path


def get_path_config() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.ConfigLocation)
    return path


def get_path_download() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.DownloadLocation)
    return path


def get_path_generic_cache() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.GenericCacheLocation)
    return path


def get_path_generic_config() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.GenericConfigLocation)
    return path


def get_path_app_data() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.AppDataLocation)
    return path


def get_path_app_config() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.AppConfigLocation)
    return path


def get_path_app_local_data() -> str:
    path = QStandardPaths.writableLocation(QStandardPaths.AppLocalDataLocation)
    return path